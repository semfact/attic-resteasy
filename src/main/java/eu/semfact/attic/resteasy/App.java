package eu.semfact.attic.resteasy;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.util.FileManager;
import eu.semfact.attic.graphstore.StoreFactory;
import eu.semfact.attic.graphstore.tdb.PersistentTDBStoreFactory;
import eu.semfact.attic.resteasy.resource.Container;
import eu.semfact.attic.resteasy.resource.Index;
import eu.semfact.attic.storage.RepresentationFactory;
import eu.semfact.attic.storage.h2.H2RepresentationFactory;
import eu.semfact.webid.resteasy.TDBWebIDInterceptor;
import eu.semfact.webid.resteasy.netty.WebIDNettyJaxrsServer;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jboss.resteasy.spi.ResteasyDeployment;

public class App {
    
    public static void main(String[] args) throws Exception {
        OptionBuilder.withType(Number.class);
        OptionBuilder.hasArg();
        OptionBuilder.withArgName("number");
        OptionBuilder.withDescription("the port to bind to");
        Option portopt = OptionBuilder.create("port");
        
        Options options = new Options();
        options.addOption(portopt);

        CommandLineParser parser = new GnuParser();
        try {
            CommandLine line = parser.parse(options, args);
            
            // create deployment for RESTEasy
            ResteasyDeployment deployment = new ResteasyDeployment();
            deployment.setResourceClasses(getResources());
            deployment.setProviderClasses(getProviders());
            deployment.setDefaultContextObjects(getContext());
            // create WebID server with Netty and JAX-RS
            final WebIDNettyJaxrsServer server = new WebIDNettyJaxrsServer();
            // set deployment
            server.setDeployment(deployment);
            // set key manager
            server.setKeyManagers(getKeyManager());
            // default port is 3000
            int port = 3000;
            // use port from command line if given
            if (line.hasOption("port")) {
                port = Integer.parseInt(line.getOptionValue("port"));
            }
            // set the port
            server.setPort(port);
            // start the server
            server.start();
            // inform the user
            System.out.println("Server listening on port " + port);
        } catch (ParseException exp) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("memori", options);
        }
    }

    /*
     * Returns a list of classes that implement jax-rs resources for this
     * application.
     */
    protected static List<String> getResources() {
        List<String> res = new ArrayList<>();
        res.add(Index.class.getName());
        res.add(Container.class.getName());
        return res;
    }

    /*
     * Returns a list of classes that should be used as providers by
     * RESTEasy.
     */
    protected static List<String> getProviders() {
        return Collections.singletonList(TDBWebIDInterceptor.class.getName());
    }

    /*
     * Returns the context that will be used for this application.
     */
    protected static Map<Class, Object> getContext() throws SQLException {
        HashMap<Class, Object> context = new HashMap<>();
        StoreFactory sf = new PersistentTDBStoreFactory();
        String file = App.class.getClassLoader().getResource("base.ttl").toString();
        Model base = FileManager.get().loadModel(file);
        sf.getStorage().getDefaultModel().add(base);
        context.put(RepresentationFactory.class, new H2RepresentationFactory());
        context.put(StoreFactory.class, sf);
        return context;
    }

    protected static KeyManager[] getKeyManager() throws NoSuchAlgorithmException, FileNotFoundException, KeyStoreException, IOException, UnrecoverableKeyException, CertificateException {
        TrustManagerFactory tmFactory = TrustManagerFactory.getInstance("PKIX");
        KeyStore tmpKS = null;
        tmFactory.init(tmpKS);
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(App.class.getClassLoader().getResourceAsStream("cert.jks"), "secret".toCharArray());
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, "secret".toCharArray());
        return kmf.getKeyManagers();
    }
}
