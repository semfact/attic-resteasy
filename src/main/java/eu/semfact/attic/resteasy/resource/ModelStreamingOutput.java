
package eu.semfact.attic.resteasy.resource;

import com.hp.hpl.jena.rdf.model.Model;
import java.io.IOException;
import java.io.OutputStream;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;

public class ModelStreamingOutput implements StreamingOutput {

    protected Model m;
    protected String lang;
    
    public ModelStreamingOutput (Model m, MediaType t) {
        this.m = m;
        switch(t.getSubtype()) {
            case "turtle":
                lang = "TURTLE";
            break;
            case "n3":
                lang = "N3";
            break;
            case "rdf+xml":
                lang = "RDF/XML";
            break;
            default:
                throw new IllegalStateException("MediaType "+t.toString()+" not supported by "+ModelStreamingOutput.class.getName());
        }
    }
    
    @Override
    public void write(OutputStream out) throws IOException, WebApplicationException {
        m.write(out, lang);
    }
    
}
