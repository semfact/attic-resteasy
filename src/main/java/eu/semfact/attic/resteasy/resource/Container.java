package eu.semfact.attic.resteasy.resource;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import eu.semfact.attic.graphstore.StoreFactory;
import eu.semfact.attic.storage.Representation;
import eu.semfact.attic.storage.RepresentationFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Variant;
import org.apache.commons.io.IOUtils;

@Path("/{container}")
public class Container {

    @Context
    protected RepresentationFactory rf;
    @Context
    protected StoreFactory sf;
    protected SecureRandom random = new SecureRandom();
    
    @GET
    public Response get(
            @PathParam("container") String container,
            @Context UriInfo uri,
            @Context Request req) {
        ResponseBuilder resp;
        Dataset storage = sf.getStorage();
        String uristr = uri.getBaseUri().toASCIIString();
        uristr += container + "/";
        Model c = QueryExecutionFactory.create("DESCRIBE <"+uristr+">", storage.getDefaultModel()).execDescribe();
        if (storage.containsNamedModel(uristr)) {
            c = c.union(storage.getNamedModel(uristr));
        }
        List<Variant> variants;
        variants = Variant.encodings(
                "gzip",
                "deflate")
                .mediaTypes(
                MediaType.valueOf("text/turtle"),
                MediaType.valueOf("text/n3"),
                MediaType.valueOf("application/rdf+xml")).build();
        
        Variant variant = req.selectVariant(variants);
   
        if (variant != null) {
            StreamingOutput so = new ModelStreamingOutput(c, variant.getMediaType());
            return Response.ok(so).variant(variant).build();
        } else {
            resp = Response.notAcceptable(variants);
            return resp.build();
        }
    }

    @POST
    public Response post(
            @HeaderParam("Content-Type") MediaType type,
            @HeaderParam("Content-Language") Locale loc,
            @PathParam("container") String container,
            @Context UriInfo uri,
            InputStream body) throws IOException {
        ResponseBuilder resp;
        Variant variant;
        Representation rep;
        String containeruri = uri.getBaseUriBuilder().path("/" + container + "/").build().toASCIIString();
        if (isContainer(containeruri)) {
            String path = "/" + container + "/" + new BigInteger(130, random).toString(32).substring(0, 20);
            if (loc == null) {
                loc = Locale.ROOT;
            }
            variant = new Variant(type, loc, "deflate");
            rep = rf.createRepresentation(path, variant);
            IOUtils.copy(body, rep.getOutputStream());
            resp = Response.status(Response.Status.CREATED);
            resp.tag(rep.getEntityTag());
            resp.location(uri.getBaseUriBuilder().path(path).build());
        } else {
            resp = Response.status(Response.Status.NOT_FOUND);
        }
        return resp.build();
    }

    @GET
    @Path("/{path:.*}")
    public Response getContainerResource(
            @Context Request req,
            @Context UriInfo uri,
            InputStream body) throws IOException {
        ResponseBuilder resp;
        Variant selected;
        List<Variant> variants;
        Representation rep;
        String path = uri.getPath();
        variants = rf.getVariants(path);
        if (variants.isEmpty()) {
            resp = Response.status(Response.Status.NOT_FOUND);
            return resp.build();
        }
        selected = req.selectVariant(variants);
        if (selected == null) {
            resp = Response.notAcceptable(variants);
            return resp.build();
        }
        rep = rf.selectRepresentation(path, selected);
        resp = req.evaluatePreconditions(rep.getLastModified(), rep.getEntityTag());
        if (resp != null) {
            return resp.build();
        } else {
            resp = Response.ok(rep.getInputStream(), selected);
            resp.tag(rep.getEntityTag());
            resp.lastModified(rep.getLastModified());
            resp.variants(variants);
            return resp.build();
        }
    }

    @PUT
    @Path("/{path:.*}")
    public Response put(
            @Context Request req,
            @HeaderParam("Content-Type") MediaType type,
            @HeaderParam("Content-Language") Locale loc,
            @Context UriInfo uri,
            InputStream body) throws IOException {
        ResponseBuilder resp;
        Variant variant;
        List<Variant> variants;
        Representation rep;
        String path = uri.getPath();
        if (loc == null) {
            loc = Locale.ROOT;
        }
        variant = new Variant(type, loc, "deflate");
        rep = rf.selectRepresentation(path, variant);
        if (rep == null) {
            variants = rf.getVariants(path);
            if (variants.isEmpty()) {
                resp = Response.noContent().status(Response.Status.CONFLICT);
            } else {
                rep = rf.createRepresentation(path, variant);
                IOUtils.copy(body, rep.getOutputStream());
                resp = Response.noContent().status(Response.Status.OK);
                resp.tag(rep.getEntityTag());
            }
        } else {
            resp = req.evaluatePreconditions(rep.getLastModified(), rep.getEntityTag());
            if (resp == null) {
                IOUtils.copy(body, rep.getOutputStream());
                resp = Response.noContent().status(Response.Status.OK);
                resp.tag(rep.getEntityTag());
            }
        }
        return resp.build();
    }

    protected boolean isContainer(String path) {
        Dataset graph = sf.getStorage();
        Resource s = ResourceFactory.createResource(path);
        Property p = ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        Resource o = ResourceFactory.createResource("http://open-services.net/ns/basicProfile#Container");
        return graph.getDefaultModel().contains(s, p, o);
    }
}
