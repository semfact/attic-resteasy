
package eu.semfact.attic.resteasy.resource;

import eu.semfact.attic.graphstore.StoreFactory;
import eu.semfact.attic.storage.RepresentationFactory;
import java.util.Collection;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.spi.HttpRequest;

@Path("/")
public class Index {
    
    @Context
    protected RepresentationFactory rf;
    @Context
    protected StoreFactory sf;
    
    @GET
    @Produces("text/plain; charset=UTF-8")
    public Response get(@Context HttpRequest req) {
        String str;
        str = "Attic\n\n";
        Object claims = req.getAttribute("webidclaims");
        Object webids = req.getAttribute("webids");
        if (claims != null) {
            Collection<String> uris = (Collection<String>) claims;
            Collection<String> ids  = (Collection<String>) webids;
            str += "You claimed the following WebIDs:\n";
            int i = 1;
            for (String uri : uris) {
                str += i + ". " + uri + "\n";
                i++;
            }
            str += "\nThe following WebIDs have been verified:\n";
            i = 1;
            for (String uri : ids) {
                str += i + ". " + uri + "\n";
                i++;
            }
        }
        return Response.status(200)
                       .entity(str)
                       .build();
    }
    
    @POST
    public Response post() {
        return null;
    }
    
}
